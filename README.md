## 安装
```shell
composer require zhen/hyperf-kit
```

### 如果需要使用 表单验证器，需要做以下操作

> 参考：https://hyperf.wiki/2.2/#/zh-cn/validation

**添加中间件**

您需要为使用到验证器组件的 Server 在 `config/autoload/middlewares.php` 配置文件加上一个全局中间件 `Hyperf\Validation\Middleware\ValidationMiddleware` 的配置，如下为 `http` Server 加上对应的全局中间件的示例：

```php
<?php
return [
    // 下面的 http 字符串对应 config/autoload/server.php 内每个 server 的 name 属性对应的值，意味着对应的中间件配置仅应用在该 Server 中
    'http' => [
        // 数组内配置您的全局中间件，顺序根据该数组的顺序
        \Hyperf\Validation\Middleware\ValidationMiddleware::class
        // 这里隐藏了其它中间件
    ],
];
```

**添加异常处理器**

`config/autoload/exceptions.php`

```php
return [
    'handler' => [
        'http' => [
            \Zhen\HyperfKit\Exception\Handler\ValidationExceptionHandler::class,
        ],
    ],
];
```

**发布验证器语言文件**

```shell
php bin/hyperf.php vendor:publish hyperf/translation
php bin/hyperf.php vendor:publish hyperf/validation
```

## 异常处理说明

当 `.env` 中的 `APP_ENV=dev` 或 `APP_DEBUG=true` 表示当前为开发环境，会将异常详细信息返回，不会走其他逻辑处理