<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/11/12 16:38,
 * @LastEditTime: 2022/11/12 16:38
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Helper;

use Hyperf\Context\Context;
use Hyperf\HttpServer\Contract\RequestInterface;

class LoginUser
{
    protected const USER_INFO_KEY = 'loginUser.userinfo';
    protected const REFRESH_TOKEN_KEY = 'loginUser.refresh_token';
    protected const BEARER_TOKEN_KEY = 'loginUser.bearer_token';

    protected RequestInterface $request;

    /**
     * 设置用户信息
     * @param array $userInfo
     */
    public static function setUserInfo(array $userInfo)
    {
        Context::set(self::USER_INFO_KEY, $userInfo);
    }

    /**
     * 获取用户信息
     * @return array
     */
    public static function getUserInfo(): array
    {
        return Context::get(self::USER_INFO_KEY) ?: [];
    }

    /**
     * 获取 Bearer Token
     * @return string|null
     */
    public static function getBearerAuth(): ?string
    {
        return Context::get(self::BEARER_TOKEN_KEY) ?? container()->get(RequestInterface::class)->header('Authorization');
    }

    /**
     * 设置 Bearer Token
     */
    public static function setBearerAuth(string $token): void
    {
        Context::set(self::BEARER_TOKEN_KEY, $token);
    }

    /**
     * 设置刷新的token
     * @param string $token
     */
    public static function setRefreshToken(string $token): void
    {
        Context::set(self::REFRESH_TOKEN_KEY, $token);
    }

    /**
     * 获取刷新的token
     * @return string|null
     */
    public static function getRefreshToken(): ?string
    {
        return Context::get(self::REFRESH_TOKEN_KEY);
    }
}