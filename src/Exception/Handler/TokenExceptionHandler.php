<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 11:45,
 * @LastEditTime: 2022/10/07 11:45
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Exception\Handler;


use Hyperf\ExceptionHandler\ExceptionHandler;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Zhen\HyperfKit\CoreResponse;
use Zhen\HyperfKit\Exception\TokenException;

class TokenExceptionHandler extends ExceptionHandler
{

    /**
     * Handle the exception, and return the specified result.
     */
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();
        return make(CoreResponse::class)->tokenError($throwable->getMessage());
    }

    /**
     * Determine if the current exception handler should handle the exception.
     *
     * @return bool
     *              If return true, then this exception handler will handle the exception,
     *              If return false, then delegate to next handler
     */
    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof TokenException;
    }
}