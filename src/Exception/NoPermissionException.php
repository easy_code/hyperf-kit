<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 10:59,
 * @LastEditTime: 2022/10/07 10:59
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Exception;


use Zhen\HyperfKit\Constants\ResponseCode;

class NoPermissionException extends CoreException
{
    protected $code = ResponseCode::NO_PERMISSION;

    protected $message = '无权操作';
}