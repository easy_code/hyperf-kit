<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/06 17:51,
 * @LastEditTime: 2022/10/06 17:51
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Exception;

class CoreException extends \RuntimeException
{
    /**
     * 是否返回异常信息响应.
     */
    public bool $responseErrInfo = true;
}