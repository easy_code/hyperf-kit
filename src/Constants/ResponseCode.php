<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/06 18:09,
 * @LastEditTime: 2022/10/06 18:09
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * 业务全局状态码
 * Class ResponseCode
 * @author lwz
 */
#[Constants]
class ResponseCode extends AbstractConstants
{
    /**
     * @Message("成功")
     */
    public const SUCCESS = 0;

    /**
     * @Message("服务器繁忙，请稍后再试！")
     */
    public const SERVER_ERROR = 500;

    /**
     * @Message("身份验证错误，请重新登录")
     */
    public const TOKEN_EXPIRED = 1001;

    /**
     * @Message("无权操作")
     */
    public const NO_PERMISSION = 1003;

    /**
     * @Message("数据验证失败")
     */
    public const VALIDATE_FAILED = 1002 ; // 数据验证失败（如：表单）
}