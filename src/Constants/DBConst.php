<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/11/24 14:45,
 * @LastEditTime: 2022/11/24 14:45
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Constants;


class DBConst
{
    // 返回删除数据的类型
    const TRASHED_TYPE_DEFAULT = 1; // 默认（过滤已删除的数据）
    const TRASHED_TYPE_WITH = 2; // 返回包括已删除的数据
    const TRASHED_TYPE_ONLY = 3; // 只返回已删除的数据
}