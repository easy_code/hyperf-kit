<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/07 17:33,
 * @LastEditTime: 2022/10/07 17:33
 */
declare(strict_types=1);

namespace Zhen\HyperfKit\Traits;


use Hyperf\Database\Model\Collection;
use Zhen\HyperfKit\Abstracts\AbstractDao;
use Zhen\HyperfKit\CoreModel;
use Zhen\HyperfKit\Exception\CoreException;

trait ServiceTrait
{
    /**
     * @var AbstractDao
     */
    public $dao;

    /**
     * 获取列表数据.
     * @param array $where where查询条件
     * @param array|null $params
     *     select：查询的字段（数组）
     *     order_by：排序的字段。多个字段用数组
     *     order_type：排序的类型。如果指定类型，需要跟 order_by 一一对应
     *     table_alias：表别名
     *     trashed_type: 1：默认；2：获取已删除的；3：只返回已删除的
     * @param null|int $maxSize 最大数据量。传 0 不限制
     * @return Collection
     */
    public function getList(array $where, ?array $params = null, ?int $maxSize = null): Collection
    {
        return $this->dao->getList($where, $params, $maxSize);
    }

    /**
     * 获取分页列表.
     * @param array $where 查询条件
     * @param array $params 列表参数
     *     page：页码
     *     size：分页大小
     *     select：查询的字段（数组）
     *     order_by：排序的字段。多个字段用数组
     *     order_type：排序的类型。如果指定类型，需要跟 order_by 一一对应
     *     table_alias：表别名
     *     trashed_type: 1：默认；2：获取已删除的；3：只返回已删除的
     * @param bool $getTotal 是否获取数据总数
     */
    public function getPageList(array $where, array $params, bool $getTotal = false): array
    {
        return $this->dao->getPageList($where, $params, $getTotal);
    }


    /**
     * 通过ID查找一条记录.
     * @param int $id id
     * @param array $fields 获取的字段
     */
    public function getOneById(int $id, array $fields = ['*']): ?CoreModel
    {
        return $this->dao->getOneById($id, $fields);
    }

    /**
     * 通过where查找一条记录.
     * @param array|string[] $fields
     */
    public function getOne(array $where, array $fields = ['*']): ?CoreModel
    {
        return $this->dao->getOne($where, $fields);
    }

    /**
     * 获取数据总数.
     * @param array $where 查询条件
     */
    public function getCount(array $where): int
    {
        return $this->dao->getCount($where);
    }

    /**
     * 新增数据（一条）.
     */
    public function save(array $data): CoreModel
    {
        return $this->dao->save($data);
    }

    /**
     * 批量插入数据.
     */
    public function insert(array $data): bool
    {
        return $this->dao->insert($data);
    }

    /**
     * 更新或添加数据.
     * @param array $where 查询条件
     * @param array $data 更新的数据
     */
    public function updateOrCreate(array $where, array $data): CoreModel
    {
        return $this->dao->updateOrCreate($where, $data);
    }

    /**
     * 更新数据.
     * @param array $where 更新条件
     * @param array $data 更新的数据
     * @param bool $withTrashed 是否更新被删除的数据
     * @return int
     */
    public function update(array $where, array $data, bool $withTrashed = false): int
    {
        return $this->dao->update($where, $data, $withTrashed);
    }

    /**
     * 批量更新.
     * @param array $where 查询条件
     * @param array $updateData 更新的数据（更新单个字段，传一维数组；更新多个字段，传二维数组）
     *                          [
     *                          'field' => '更新的字段',
     *                          'case_data' => [
     *                          ['when_field' => '判断字段', 'when_val' => '判断的值', 'update_val' => '更新的值']
     *                          ['when_raw' => 'when的完整条件', 'update_val' => '更新的值']
     *                          ]
     *                          ]
     * @param bool $withTrashed 是否更新被删除的数据
     */
    public function updateBatch(array $where, array $updateData, bool $withTrashed = false): int
    {
        return $this->dao->updateBatch($where, $updateData, $withTrashed);
    }

    /**
     * 根据 where column自增num.
     * @param array $where 查询条件
     * @param string $column 字段
     * @param int $num 自增数量
     */
    public function increment(array $where, string $column, int $num = 1): int
    {
        return $this->dao->increment($where, $column, $num);
    }

    /**
     * 根据 where column自减 num.
     * @param array $where 查询条件
     * @param string $column 字段
     * @param int $num 自增数量
     */
    public function decrement(array $where, string $column, int $num = 1): int
    {
        return $this->dao->decrement($where, $column, $num);
    }

    /**
     * 根据where 删除多条记录.
     * @param array $where 查询条件
     * @return int 删除的数据条数
     * @throws CoreException|\Exception
     */
    public function deleteByWhere(array $where): int
    {
        return $this->dao->deleteByWhere($where);
    }

    /**
     * 强制删除全部数据（包括软删除数据）.
     * @param array $where 查询条件
     * @return bool|null
     */
    public function forceDeleteAllData(array $where): ?bool
    {
        return $this->dao->forceDeleteAllData($where);
    }
}